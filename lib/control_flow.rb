# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |letter|
    if letter == letter.downcase
      str.delete!(letter)
    end
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.even?
    str[str.length / 2 - 1] + str[(str.length/2)]
  elsif str.length.odd?
    str[str.length / 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  ctr = 0
  str.each_char do |letter|
    if VOWELS.include?(letter)
      ctr += 1
    end
  end
  ctr
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  (1..num).each do |current_num|
    product *= current_num
  end
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  return_str = ""
  arr.each_index do |idx|
    if idx != arr.length - 1
      return_str += arr[idx] + separator
    elsif idx == arr.length - 1
      return_str += arr[idx]
    end
  end
  return_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_word = ""
  str.each_char.with_index do |letter, idx|
    if idx.even?
      weird_word += letter.downcase
    elsif idx.odd?
      weird_word += letter.upcase
    end
  end
  weird_word
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split(" ")
  words.each_index do |idx|
    if words[idx].length >= 5
      words[idx] = words[idx].reverse
    end
  end
  words.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizz_arr = (1..n).to_a
  fizz_arr.each_with_index do |num, idx|
    fizz_arr[idx] = "fizz" if num % 3 == 0
    fizz_arr[idx] = "buzz" if num % 5 == 0
    fizz_arr[idx] = "fizzbuzz" if num % (3 * 5) == 0
  end
  fizz_arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reverse_arr = []
  arr.each do |ele|
    reverse_arr.unshift(ele)
  end
  reverse_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  (2..Math.sqrt(num)).none? {|current_num| num % current_num == 0}
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factor_arr = []
  (1..num).each do |current_num|
    factor_arr.push(current_num) if num % current_num == 0
  end
  factor_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factor_arr = []
  (1..num).each do |current_num|
    prime_factor_arr.push(current_num) if num % current_num == 0 && prime?(current_num)
  end
  prime_factor_arr
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def mostly_even(arr)
  even_count = 0
  arr.each do |ele|
    even_count += 1 if ele.even?
    break if even_count == 2
  end
  even_count > 1
end

def oddball(arr)
  if mostly_even(arr)
    arr.each do |ele|
      return ele if ele.odd?
    end
  elsif !mostly_even(arr)
    arr.each do |ele|
      return ele if ele.even?
    end
  end
end
